from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Detail(models.Model):
	Salary=models.CharField(max_length=50)
	City=models.CharField(max_length=50)
	Name=models.CharField(max_length=100)
	Rollno=models.CharField(max_length=100)
	sno=models.CharField(max_length=20)
	Age=models.CharField(max_length=50)
	Zipcode=models.CharField(max_length=100)
	Phone=models.CharField(max_length=100)
	State=models.CharField(max_length=15)
	Address=models.CharField(max_length=255)

	def __unicode__(self):
		return self.Name
